(ns downvote-bot.core
  (:gen-class)
  (:require
   [clojure.core.async :as async]
   [clojure.string :as s]
   [clojure.java.shell :refer [sh]]
   [discljord.connections :as conn]
   [discljord.messaging :as mesg]
   [discljord.events :as events]
   [clojure.string :as str]

   [downvote-bot.praise :as praise]
   [downvote-bot.bigmoji :as bigmoji]
   [downvote-bot.turnips :as turnips]))

;; The Discord bot token
(def token (s/trim (slurp "token.txt")))

;; The downvote emoji, which for some reason also has a face for some reason
(def downvote-emoji "⬇️")

;; The number of downvotes needed to delete a message
(def delete-amount 5)

;; Prefix that all commands start with
(def prefix ")")

;; Application state, shouldn't ever change in this case
(def state (atom nil))

;; Handles commands, dispatches based on the first word connected to the prefix
(defmulti handle-command :command)

;; Default command handler, does nothing but keeps compatibility with quote-bot
(defmethod handle-command :default [one])

;; Handler for adding a BigMoji
(defmethod handle-command :register
  [{:keys [channel-id content]}]
  (let [split (s/split content #" ")
        name (bigmoji/parse-reg (second split))]
    (if name
      (do
        (bigmoji/register! name (s/join " " (rest (rest split))))
        (mesg/create-message!
         (:mesg-ch @state)
         channel-id
         :content (apply str "Created new BigEmoji: " name)))
      (mesg/create-message!
       (:mesg-ch @state)
       channel-id
       :content
       "Invalid name"))))

;; Recalls the list of bigmojis for the user
(defmethod handle-command :recall
  [{:keys [channel-id]}]
  (mesg/create-message!
   (:mesg-ch @state)
   channel-id
   :content (s/join ", " (bigmoji/recall))))

;; Handler for praising an idol
(defmethod handle-command :praise
  [{:keys [channel-id content]}]
  (mesg/create-message!
   (:mesg-ch @state)
   channel-id
   :content (if (contains? praise/praisable (str/lower-case content))
              "Your prayer has been heard..."
              (s/join (list
                       "Blasphmer! "
                       content
                       " is not one of the approved idols!")))))

;; Returns the list of praisable idols
(defmethod handle-command :praisable
  [{:keys [channel-id]}]
  (mesg/create-message!
   (:mesg-ch @state)
   channel-id
   :content (s/join ", " praise/praisable)))

;; Returns a random word, or words, from the system dictionary
(defmethod handle-command :word
  [{:keys [channel-id content]}]
  (mesg/create-message!
   (:mesg-ch @state)
   channel-id
   :content
   (try (let [n (min (Integer/parseInt (or content "1")) 15)]
          (s/trim
           (:out
            (sh
             "shuf"
             "-n"
             (str n)
             "/usr/share/dict/words"))))
        (catch Exception _ "Not a number"))))

;; Records a turnip price for a user
(defmethod handle-command :t
  [{{author-id :id} :author
    message-id :id
    :keys [channel-id content]}]
  (when (number? (read-string content))
    (turnips/add-price! author-id (read-string content))
    (mesg/create-reaction!
     (:mesg-ch @state)
     channel-id
     message-id
     "👌")))

;; Recalls the last week of turnip prices for a user
;; TODO allow a mention to be used instead of the author
(defmethod handle-command :history
  [{{author-id :id} :author :keys [channel-id]}]
  (let [prices (->> (turnips/load-prices)
                    (turnips/this-week)
                    (turnips/user-prices author-id))]
    (mesg/create-message!
     (:mesg-ch @state)
     channel-id
     :embed
     {:title "The last week of recorded Turnip prices"
      :fields
      (reduce
       #(conj %1 {:name (turnips/nice-time (first %2))
                  :value (last (last %2))})
       []
       prices)})))


;; Create the mulimethod for dispatch


(defmulti handle-event
  (fn [event-type event-data]
    event-type))

;; Generic handler for all other events
(defmethod handle-event :default
  [event-type event-data])

(defn- command-parser [content]
  (keyword (s/replace-first
            (first (s/split content #" "))
            prefix
            "")))

(defn- content-cleaner [content]
  (let [c (s/trim (s/replace content #"\)\w+" ""))]
    (when-not (s/blank? c)
      c)))

(defn- trace-message [message]
  (clojure.pprint/pprint message)
  message)

;; Handler for incoming messages written by users
(defmethod handle-event :message-create
  [event-type {{bot :bot} :author
               :keys [message-id channel-id content] :as message}]
  (when-not bot
    ;; Specifically scan for bigmoji callouts in messages
    (let [bigmoji-matches (bigmoji/parse-mesg (s/trim content))]
      (when (not-empty bigmoji-matches)
        (mesg/create-message!
         (:mesg-ch @state)
         channel-id
         :content (bigmoji/retrieve
                   (first bigmoji-matches)))))
    ;; Otherwise handle the command
    (if (s/starts-with? content prefix)
      (handle-command
       (assoc message
              :command (command-parser content)
              :content (content-cleaner content))))))

;; Handler for reaction add events used for downvotes
(defmethod handle-event :message-reaction-add
  [event-type {{name :name} :emoji :keys [message-id channel-id]}]
  (cond (and (= name downvote-emoji)
             (<= delete-amount
                 (count
                  @(mesg/get-reactions!
                    (:mesg-ch @state) channel-id message-id downvote-emoji))))
        (do
          (mesg/delete-message! (:mesg-ch @state) channel-id message-id)
          (mesg/create-message!
           (:mesg-ch @state)
           channel-id
           :content "Message Deleted. Get fucked."))

        ;; Delete message if THE WHEEL DISAPPROVES
        (= name "wheelsealofDISapproval")
        (do
          (mesg/delete-message! (:mesg-ch @state) channel-id message-id)
          (mesg/create-message!
           (:mesg-ch @state)
           channel-id
           :content "**THE WHEEL DISAPPROVES**"))))

(defn -main
  "Creates the bot and runs it"
  [& args]
  ;; Create connections, storing them for a bit
  (let [event-channel (async/chan 100)
        connection-channel (conn/connect-bot! token event-channel)
        messaging-channel (mesg/start-connection! token)]
    ;; Set the default state
    (reset! state {:conn-ch connection-channel
                   :evt-ch event-channel
                   :mesg-ch messaging-channel})
    ;; Begin the main loop for the event-handlers
    (events/message-pump! event-channel handle-event)
    ;; Cleanup when everything is done
    (mesg/stop-connection! messaging-channel)
    (conn/disconnect-bot! connection-channel)))
