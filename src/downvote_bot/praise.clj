(ns downvote-bot.praise)

(def praisable
  #{"sisman"
    "sis man"
    "trackman"
    "the trackman"
    "`the trackman`"
    "gigantor"
    "toyotathon"
    "forzathon"
    "ж"
    "skitchin"
    "skitchin'"
    "bob esponja"
    "howard dean"
    "2004 presidential candidate howard dean"
    "gooper"
    "gumshoe gooper"
    "the wall"
    "the **wall**"})
