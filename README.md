# Downvote-Bot

A [Discord](https://discordapp.com) bot that deletes messages that have been
downvoted 3 times.

```sh
$ java -jar downvote-bot-0.1.0-standalone.jar
```

## License

Copyright © 2020 Steven vanZyl

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
